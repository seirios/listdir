include os_detect.mk

CC := gcc --std=c99

MV ?= mv -f
CP ?= cp -f
LN ?= ln -sf
INSTALL ?= install
STRIP ?= strip

BINALIAS := lf
BINTARGET := listdir
OBJFILES := listdir.o
SRCFILES := listdir.c

ifeq (Darwin,$(DETECTED_OS))
PREFIX ?= /usr/local
else
PREFIX ?= /usr
endif
BINDIR ?= $(PREFIX)/bin

.PHONY: all remake
all: $(BINALIAS)
remake: clean all

$(BINALIAS): $(BINTARGET)
	$(LN) $(realpath $(BINTARGET)) $(BINALIAS)

$(BINTARGET): $(OBJFILES)

$(OBJFILES): CFLAGS += -Wall -Wextra -Werror -pedantic -pedantic-errors
ifeq (Darwin,$(DETECTED_OS))
# for DT_* constants
$(OBJFILES): CPPFLAGS += -D_DARWIN_SOURCE
else
# for strdup
$(OBJFILES): CPPFLAGS += -D_POSIX_C_SOURCE=200809L
# for DT_* constants
$(OBJFILES): CPPFLAGS += -D__USE_MISC=1
# for strcasecmp
$(OBJFILES): CPPFLAGS += -D_GNU_SOURCE
endif
$(OBJFILES): CFLAGS += -O2

.PHONY: maintainer-update-src
maintainer-update-src:
	(cd src && make) && \
		$(CP) $(addprefix src/,$(SRCFILES)) .

.PHONY: maintainer-update m-u
maintainer-update m-u: maintainer-update-src
	$(MAKE) remake

.PHONY: clean clean-all
clean:
	$(RM) $(BINTARGET) $(BINALIAS) $(OBJFILES)

clean-all: clean
	$(RM) listdir.c

.PHONY: install
install: $(BINDIR)/lf

$(BINDIR)/lf: listdir
	$(INSTALL) $< $@
	$(STRIP) $@
