/*
listdir, a.k.a lf: list files fast!
Copyright (C) 2022-2024  Sirio Bolaños Puchet

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include <dirent.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>

#include <regex.h>

#define N1 16             // array size for DT_* constants used as indices
#define ANSI_COLOR_LEN 16 // max chars to describe ANSI color escape code



#include <stdio.h>

#ifndef info0
#define info0(M) ((false) ? 0\
        : ((false)\
            ? fprintf(stderr,"%s[%s] %5s: "  M "\x1B[39m" "\n","\x1B[39m","listdir", "INFO"   )\
            : fprintf(stderr,"[%s] %5s: "  M "\n","listdir", "INFO"   )))
#endif
#ifndef warn0
#define warn0(M) ((false) ? 0\
        : ((false)\
            ? fprintf(stderr,"%s[%s] %5s: "  M "\x1B[39m" "\n","\x1B[38;5;11m","listdir", "WARN"   )\
            : fprintf(stderr,"[%s] %5s: "  M "\n","listdir", "WARN"   )))
#endif
#ifndef error0
#define error0(M) ((false) ? 0\
        : ((false)\
            ? fprintf(stderr,"%s[%s] %5s: "  M "\x1B[39m" "\n","\x1B[38;5;9m","listdir", "ERROR"   )\
            : fprintf(stderr,"[%s] %5s: "  M "\n","listdir", "ERROR"   )))
#endif
#ifndef verbose0
#define verbose0(M) ((false) ? 0\
        : ((false)\
            ? fprintf(stderr,"%s[%s] %5s: "  M "\x1B[39m" "\n","\x1B[38;5;12m","listdir", "VERB"   )\
            : fprintf(stderr,"[%s] %5s: "  M "\n","listdir", "VERB"   )))
#endif
#ifndef debug0
#define debug0(M) ((false) ? 0\
        : ((false)\
            ? fprintf(stderr,"%s[%s] %5s: " "%s: " M "\x1B[39m" "\n","\x1B[38;5;13m","listdir", "DEBUG" , __func__ )\
            : fprintf(stderr,"[%s] %5s: " "%s: " M "\n","listdir", "DEBUG" , __func__ )))
#endif
#ifndef info
#define info(M,...) ((false) ? 0\
        : ((false)\
            ? fprintf(stderr,"%s[%s] %5s: "  M "\x1B[39m" "\n","\x1B[39m","listdir", "INFO"   ,__VA_ARGS__)\
            : fprintf(stderr,"[%s] %5s: "  M "\n","listdir", "INFO"   ,__VA_ARGS__)))
#endif
#ifndef warn
#define warn(M,...) ((false) ? 0\
        : ((false)\
            ? fprintf(stderr,"%s[%s] %5s: "  M "\x1B[39m" "\n","\x1B[38;5;11m","listdir", "WARN"   ,__VA_ARGS__)\
            : fprintf(stderr,"[%s] %5s: "  M "\n","listdir", "WARN"   ,__VA_ARGS__)))
#endif
#ifndef error
#define error(M,...) ((false) ? 0\
        : ((false)\
            ? fprintf(stderr,"%s[%s] %5s: "  M "\x1B[39m" "\n","\x1B[38;5;9m","listdir", "ERROR"   ,__VA_ARGS__)\
            : fprintf(stderr,"[%s] %5s: "  M "\n","listdir", "ERROR"   ,__VA_ARGS__)))
#endif
#ifndef verbose
#define verbose(M,...) ((false) ? 0\
        : ((false)\
            ? fprintf(stderr,"%s[%s] %5s: "  M "\x1B[39m" "\n","\x1B[38;5;12m","listdir", "VERB"   ,__VA_ARGS__)\
            : fprintf(stderr,"[%s] %5s: "  M "\n","listdir", "VERB"   ,__VA_ARGS__)))
#endif
#ifndef debug
#define debug(M,...) ((false) ? 0\
        : ((false)\
            ? fprintf(stderr,"%s[%s] %5s: " "%s: " M "\x1B[39m" "\n","\x1B[38;5;13m","listdir", "DEBUG" , __func__ ,__VA_ARGS__)\
            : fprintf(stderr,"[%s] %5s: " "%s: " M "\n","listdir", "DEBUG" , __func__ ,__VA_ARGS__)))
#endif

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <stdint.h>
#include <math.h>
#include <limits.h>
#include <inttypes.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <getopt.h>
#include <errno.h>
#include <limits.h>
#include <math.h>











void print_usage(void) {
    /* ASCII goodness thanks to FIGlet with font smslant */
    puts("   ___     __     ___    ");
    puts("  / (_)__ / /____/ (_)___");
    puts(" / / (_-</ __/ _  / / __/");
    puts("/_/_/___/\\__/\\_,_/_/_/   ");
    puts("=========================");
    puts("LIST FILES FAST! a.k.a lf");
    putchar('\n');
        
    {
    printf("Available parameters: (* = required) (+ = multiple) [default]\n");

    const int maxlen = 16;
    (void)maxlen;

    
            printf("%s-%c, --%-*s %s%s " ":" " %s%s" "\n",
               "      ",'a',
               maxlen,"all",
               "     ",
               "  ",
               "do not ignore entries starting with .",
               " [" "false" "]");
           printf("%s-%c, --%-*s %s%s " ":" " %s%s" "\n",
               "      ",'A',
               maxlen,"almost-all",
               "     ",
               "  ",
               "do not list implied . and ..",
               " [" "false" "]");
           printf("%s-%c, --%-*s %s%s " ":" " %s%s" "\n",
               "      ",'I',
               maxlen,"case-insensitive",
               "     ",
               "  ",
               "do case-insensitive matching",
               " [" "false" "]");
           printf("%s-%c, --%-*s %s%s " ":" " %s%s" "\n",
               "      ",'F',
               maxlen,"classify",
               "     ",
               "  ",
               "print file type indicator",
               " [" "false" "]");
           printf("%s-%c, --%-*s %s%s " ":" " %s%s" "\n",
               "      ",'C',
               maxlen,"color",
               "     ",
               "  ",
               "use colors in output",
               " [" "false" "]");
           printf("%s-%c, --%-*s %s%s " ":" " %s%s" "\n",
               "      ",'c',
               maxlen,"count-only",
               "     ",
               "  ",
               "print total file count only",
               " [" "false" "]");
           printf("%s-%c, --%-*s %s%s " ":" " %s%s" "\n",
               "      ",'e',
               maxlen,"exclude",
               "<arg>",
               "  ",
               "exclude matching file names",
               "");
           printf("%s-%c, --%-*s %s%s " ":" " %s%s" "\n",
               "      ",'E',
               maxlen,"extended-regexp",
               "     ",
               "  ",
               "use extended regular expression",
               " [" "false" "]");
           printf("%s-%c, --%-*s %s%s " ":" " %s%s" "\n",
               "      ",'B',
               maxlen,"ignore-backups",
               "     ",
               "  ",
               "ignore files ending in ~",
               " [" "false" "]");
           printf("%s-%c, --%-*s %s%s " ":" " %s%s" "\n",
               "      ",'i',
               maxlen,"include",
               "<arg>",
               "  ",
               "include matching file names",
               "");
           printf("%s-%c, --%-*s %s%s " ":" " %s%s" "\n",
               "      ",'%',
               maxlen,"only-blk",
               "     ",
               "  ",
               "only list block devices",
               " [" "false" "]");
           printf("%s-%c, --%-*s %s%s " ":" " %s%s" "\n",
               "      ",'#',
               maxlen,"only-chr",
               "     ",
               "  ",
               "only list character devices",
               " [" "false" "]");
           printf("%s-%c, --%-*s %s%s " ":" " %s%s" "\n",
               "      ",'/',
               maxlen,"only-dir",
               "     ",
               "  ",
               "only list directories",
               " [" "false" "]");
           printf("%s-%c, --%-*s %s%s " ":" " %s%s" "\n",
               "      ",'|',
               maxlen,"only-fifo",
               "     ",
               "  ",
               "only list named pipes (FIFOs)",
               " [" "false" "]");
           printf("%s-%c, --%-*s %s%s " ":" " %s%s" "\n",
               "      ",'@',
               maxlen,"only-lnk",
               "     ",
               "  ",
               "only list symbolic links",
               " [" "false" "]");
           printf("%s-%c, --%-*s %s%s " ":" " %s%s" "\n",
               "      ",'f',
               maxlen,"only-reg",
               "     ",
               "  ",
               "only list regular files",
               " [" "false" "]");
           printf("%s-%c, --%-*s %s%s " ":" " %s%s" "\n",
               "      ",'=',
               maxlen,"only-sock",
               "     ",
               "  ",
               "only list UNIX domain sockets",
               " [" "false" "]");
           printf("%s-%c, --%-*s %s%s " ":" " %s%s" "\n",
               "      ",'o',
               maxlen,"output",
               "<arg>",
               "  ",
               "output to given file",
               "");
           printf("%s-%c, --%-*s %s%s " ":" " %s%s" "\n",
               "      ",'r',
               maxlen,"regex",
               "     ",
               "  ",
               "filter using regular expression",
               " [" "false" "]");
   
    
                printf("%s    --%-*s %s%s " ":" " %s%s" "\n",
            "      ",
            
            maxlen,"color-blk",
            "<arg>","  ","ANSI color escape code for block devices <5;3>","");
               printf("%s    --%-*s %s%s " ":" " %s%s" "\n",
            "      ",
            
            maxlen,"color-chr",
            "<arg>","  ","ANSI color escape code for character devices <5;11>","");
               printf("%s    --%-*s %s%s " ":" " %s%s" "\n",
            "      ",
            
            maxlen,"color-dir",
            "<arg>","  ","ANSI color escape code for directories <5;4>","");
               printf("%s    --%-*s %s%s " ":" " %s%s" "\n",
            "      ",
            
            maxlen,"color-fifo",
            "<arg>","  ","ANSI color escape code for named pipes (FIFOs) <5;5>","");
               printf("%s    --%-*s %s%s " ":" " %s%s" "\n",
            "      ",
            
            maxlen,"color-lnk",
            "<arg>","  ","ANSI color escape code for symbolic links <5;6>","");
               printf("%s    --%-*s %s%s " ":" " %s%s" "\n",
            "      ",
            
            maxlen,"color-reg",
            "<arg>","  ","ANSI color escape code for regular files <5;2>","");
               printf("%s    --%-*s %s%s " ":" " %s%s" "\n",
            "      ",
            
            maxlen,"color-sock",
            "<arg>","  ","ANSI color escape code for UNIX domain sockets <5;1>","");
               printf("%s    --%-*s %s%s " ":" " %s%s" "\n",
            "      ",
            
            maxlen,"help",
            "     ","  ","print usage information","");
               printf("%s    --%-*s %s%s " ":" " %s%s" "\n",
            "      ",
            
            maxlen,"no-classify",
            "     ","  ","do not print file type indicator <false>","");
               printf("%s    --%-*s %s%s " ":" " %s%s" "\n",
            "      ",
            
            maxlen,"no-color",
            "     ","  ","do not use colors in output <false>","");
               printf("%s    --%-*s %s%s " ":" " %s%s" "\n",
            "      ",
            
            maxlen,"no-fallback",
            "     ","  ","exit on output file creation failure <false>","");
               printf("%s    --%-*s %s%s " ":" " %s%s" "\n",
            "      ",
            
            maxlen,"print-count",
            "     ","  ","print total file count <false>","");
               printf("%s    --%-*s %s%s " ":" " %s%s" "\n",
            "      ",
            
            maxlen,"usage",
            "     ","  ","alias for --help","");
               printf("%s    --%-*s %s%s " ":" " %s%s" "\n",
            "      ",
            
            maxlen,"verbose",
            "     ","  ","print diagnostic messages","");
   }
}


static inline void fprint_pretty(FILE *stream, const char *name, int type,
                                 bool classify, bool use_color, char colors[static N1][ANSI_COLOR_LEN + 1]) {
    if(use_color) { // set color
        fputs("\x1B[38;",stream);
        fputs(colors[type],stream);
        fputc('m',stream);
    }
    fputs(name,stream);
    if(use_color) fputs("\x1B[39m",stream); // unset color

    if(classify) {
        switch(type) {
            case DT_REG: break;
                    case DT_DIR: fputc('/',stream); break;
                   case DT_FIFO: fputc('|',stream); break;
                   case DT_LNK: fputc('@',stream); break;
                   case DT_SOCK: fputc('=',stream); break;
                   case DT_BLK: fputc('%',stream); break;
                   case DT_CHR: fputc('#',stream); break;
                   default: fputc('?',stream);
        }
    }

    fputc('\n',stream);
}


int main(int argc, char *argv[]) {
    char custom_colors[N1][ANSI_COLOR_LEN + 1] = { 0 };
    unsigned long dt2bit[N1] = { 0 };

    unsigned long type_flag = 0; // filter AND types

    bool fp_fallback = true;
    bool do_print_count = false;

    /* set default colors */
    strncat(custom_colors[DT_REG],"5;2",ANSI_COLOR_LEN);
    strncat(custom_colors[DT_DIR],"5;4",ANSI_COLOR_LEN);
    strncat(custom_colors[DT_FIFO],"5;5",ANSI_COLOR_LEN);
    strncat(custom_colors[DT_LNK],"5;6",ANSI_COLOR_LEN);
    strncat(custom_colors[DT_SOCK],"5;1",ANSI_COLOR_LEN);
    strncat(custom_colors[DT_BLK],"5;3",ANSI_COLOR_LEN);
    strncat(custom_colors[DT_CHR],"5;11",ANSI_COLOR_LEN);

            
        struct {
            bool print_all; 
           bool almost_all; 
           bool ignore_bkps; 
           bool count_only; 
           bool use_color; 
           char* filter_out; 
           bool ext_regex; 
           bool classify; 
           char* filter_in; 
           bool icase; 
           char* outfile; 
           bool use_regex; 
           bool reg_only; 
           bool dir_only; 
           bool fifo_only; 
           bool lnk_only; 
           bool sock_only; 
           bool blk_only; 
           bool chr_only; 
               bool print_all_isset; 
           bool almost_all_isset; 
           bool ignore_bkps_isset; 
           bool count_only_isset; 
           bool use_color_isset; 
           bool filter_out_isset; 
           bool ext_regex_isset; 
           bool classify_isset; 
           bool filter_in_isset; 
           bool icase_isset; 
           bool outfile_isset; 
           bool use_regex_isset; 
           bool reg_only_isset; 
           bool dir_only_isset; 
           bool fifo_only_isset; 
           bool lnk_only_isset; 
           bool sock_only_isset; 
           bool blk_only_isset; 
           bool chr_only_isset; 
       } opts = { 0 };
    (void)opts;
   

    { // set option default values
            opts.print_all = false;
           opts.almost_all = false;
           opts.ignore_bkps = false;
           opts.count_only = false;
           opts.use_color = false;
               opts.filter_out = NULL;
    
           opts.ext_regex = false;
           opts.classify = false;
               opts.filter_in = NULL;
    
           opts.icase = false;
               opts.outfile = NULL;
    
           opts.use_regex = false;
           opts.reg_only = false;
           opts.dir_only = false;
           opts.fifo_only = false;
           opts.lnk_only = false;
           opts.sock_only = false;
           opts.blk_only = false;
           opts.chr_only = false;
       }
        opterr = 1;
    
    
    
                                                                                                                                                                                                                                                                                                                                                  
    if(argc == 1) { // no arguments at all
        /* noop */    } else { // process option tables
        const char *optlabel = NULL; 
        (void)optlabel;

        struct option optlist[] = {
                    { "help", no_argument, NULL, 0 }, // print usage information
                   { "usage", no_argument, NULL, 0 }, // alias for --help
                   { "verbose", no_argument, NULL, 0 }, // print diagnostic messages
                           /* do not print file type indicator <false> */
            { "no-classify", no_argument,                NULL,                0 },
                   /* do not use colors in output <false> */
            { "no-color", no_argument,                NULL,                0 },
                   /* exit on output file creation failure <false> */
            { "no-fallback", no_argument,                NULL,                0 },
                   /* print total file count <false> */
            { "print-count", no_argument,                NULL,                0 },
                   /* ANSI color escape code for regular files <5;2> */
            { "color-reg", required_argument,                NULL,                0 },
                   /* ANSI color escape code for directories <5;4> */
            { "color-dir", required_argument,                NULL,                0 },
                   /* ANSI color escape code for named pipes (FIFOs) <5;5> */
            { "color-fifo", required_argument,                NULL,                0 },
                   /* ANSI color escape code for symbolic links <5;6> */
            { "color-lnk", required_argument,                NULL,                0 },
                   /* ANSI color escape code for UNIX domain sockets <5;1> */
            { "color-sock", required_argument,                NULL,                0 },
                   /* ANSI color escape code for block devices <5;3> */
            { "color-blk", required_argument,                NULL,                0 },
                   /* ANSI color escape code for character devices <5;11> */
            { "color-chr", required_argument,                NULL,                0 },
                           /* do not ignore entries starting with . */
            { "all", no_argument, NULL, 'a' },
                   /* do not list implied . and .. */
            { "almost-all", no_argument, NULL, 'A' },
                   /* ignore files ending in ~ */
            { "ignore-backups", no_argument, NULL, 'B' },
                   /* print total file count only */
            { "count-only", no_argument, NULL, 'c' },
                   /* use colors in output */
            { "color", no_argument, NULL, 'C' },
                   /* exclude matching file names */
            { "exclude", required_argument, NULL, 'e' },
                   /* use extended regular expression */
            { "extended-regexp", no_argument, NULL, 'E' },
                   /* print file type indicator */
            { "classify", no_argument, NULL, 'F' },
                   /* include matching file names */
            { "include", required_argument, NULL, 'i' },
                   /* do case-insensitive matching */
            { "case-insensitive", no_argument, NULL, 'I' },
                   /* output to given file */
            { "output", required_argument, NULL, 'o' },
                   /* filter using regular expression */
            { "regex", no_argument, NULL, 'r' },
                   /* only list regular files */
            { "only-reg", no_argument, NULL, 'f' },
                   /* only list directories */
            { "only-dir", no_argument, NULL, '/' },
                   /* only list named pipes (FIFOs) */
            { "only-fifo", no_argument, NULL, '|' },
                   /* only list symbolic links */
            { "only-lnk", no_argument, NULL, '@' },
                   /* only list UNIX domain sockets */
            { "only-sock", no_argument, NULL, '=' },
                   /* only list block devices */
            { "only-blk", no_argument, NULL, '%' },
                   /* only list character devices */
            { "only-chr", no_argument, NULL, '#' },
                   { NULL, 0, NULL, 0 }
        };

        opterr = 0; // silent
        /* build optstring */
        const char optstring[] =
#if 1
            ":"
#endif
            "a""A""B""c""C""e:""E""F""i:""I""o:""r""f""/""|""@""=""%""#";

        int opt, longidx;
        bool getopt_verbose = false; // --verbose passed
        bool longopt_isset[14] = { 0 };
        char *longopt_arg[14] = { 0 };
        (void)longopt_isset;
(void)longopt_arg;

        while((opt = getopt_long(argc,argv,optstring,optlist,&longidx)) != -1) {
            if(opt == 0) { /* long options with no short option */
                if(longidx < 14)
                    longopt_isset[longidx] = true;
                if(longidx == 0 || longidx == 1) { // --help or --usage
                        print_usage();
    exit(EXIT_SUCCESS);
                } else if(longidx == 2) { // --verbose
                    getopt_verbose = true;
                } else if(longidx >= 3) {
                    if(longidx < 14) {
                            if(NULL != (optarg) &&       NULL == ((longopt_arg[longidx]) = strdup(optarg)))        { error0("out of memory"); exit(EXIT_FAILURE); }
                    }

                    longidx -= 3; // subtract default

                    /* set optlabel for each long option */
                    switch(longidx) {
                                            case 0:
                            optlabel = "--no-classify";
                            break;
                                           case 1:
                            optlabel = "--no-color";
                            break;
                                           case 2:
                            optlabel = "--no-fallback";
                            break;
                                           case 3:
                            optlabel = "--print-count";
                            break;
                                           case 4:
                            optlabel = "--color-reg";
                            break;
                                           case 5:
                            optlabel = "--color-dir";
                            break;
                                           case 6:
                            optlabel = "--color-fifo";
                            break;
                                           case 7:
                            optlabel = "--color-lnk";
                            break;
                                           case 8:
                            optlabel = "--color-sock";
                            break;
                                           case 9:
                            optlabel = "--color-blk";
                            break;
                                           case 10:
                            optlabel = "--color-chr";
                            break;
                                       }

                    /* user-defined actions */
                        switch(longidx) {
        case 0: // --no-classify
            opts.classify = false;
            break;
        case 1: // --no-color
            opts.use_color = false;
            break;
        case 2: // --no-fallback
            fp_fallback = false;
            break;
        case 3: // --print-count
            do_print_count = true;
            break;
            case 4: // --color-reg
            custom_colors[DT_REG][0] = '\0';
            strncat(custom_colors[DT_REG],optarg,ANSI_COLOR_LEN);
            break;
           case 5: // --color-dir
            custom_colors[DT_DIR][0] = '\0';
            strncat(custom_colors[DT_DIR],optarg,ANSI_COLOR_LEN);
            break;
           case 6: // --color-fifo
            custom_colors[DT_FIFO][0] = '\0';
            strncat(custom_colors[DT_FIFO],optarg,ANSI_COLOR_LEN);
            break;
           case 7: // --color-lnk
            custom_colors[DT_LNK][0] = '\0';
            strncat(custom_colors[DT_LNK],optarg,ANSI_COLOR_LEN);
            break;
           case 8: // --color-sock
            custom_colors[DT_SOCK][0] = '\0';
            strncat(custom_colors[DT_SOCK],optarg,ANSI_COLOR_LEN);
            break;
           case 9: // --color-blk
            custom_colors[DT_BLK][0] = '\0';
            strncat(custom_colors[DT_BLK],optarg,ANSI_COLOR_LEN);
            break;
           case 10: // --color-chr
            custom_colors[DT_CHR][0] = '\0';
            strncat(custom_colors[DT_CHR],optarg,ANSI_COLOR_LEN);
            break;
       }
                }
            }
            else if('?' == opt || ':' == opt) { // getopt error
                bool missing_arg = (':' == opt);
#define OPBUFSIZ 128
                if(optopt == 0 || optopt == true) { // long option
                    size_t oplen = 0; // length until '='
                    const char *optxt = argv[optind - 1] + 2; // start after '--'
                    for(const char *c = optxt; '\0' != *c && *c != '='; ++c, ++oplen);
                    char opbuf[oplen + 1]; // VLA
                    for(size_t i = 0; i < oplen; ++i) opbuf[i] = optxt[i];
                    opbuf[oplen] = '\0';
                        if(missing_arg) { error("option --%s requires an argument",opbuf); }
    else { error("unknown option --%s",opbuf); }

                } else { // short option
                        if(missing_arg) { error("option -%c requires an argument",optopt); }
    else { error("unknown option -%c",optopt); }

                }
                exit(EXIT_FAILURE);
            } else { /* long options with short option */
                
                switch(opt) {
                                    case 'a':
                        optlabel = "-a/--all";

                            if(opts.print_all_isset) {
        error("duplicate option %s",optlabel);
        exit(EXIT_FAILURE);
    }
                        opts.print_all_isset = true; 

                        /* option default actions, overriden by user-defined actions below */
                            opts.print_all = !false;
                    break;
                                   case 'A':
                        optlabel = "-A/--almost-all";

                            if(opts.almost_all_isset) {
        error("duplicate option %s",optlabel);
        exit(EXIT_FAILURE);
    }
                        opts.almost_all_isset = true; 

                        /* option default actions, overriden by user-defined actions below */
                            opts.almost_all = !false;
                    break;
                                   case 'B':
                        optlabel = "-B/--ignore-backups";

                            if(opts.ignore_bkps_isset) {
        error("duplicate option %s",optlabel);
        exit(EXIT_FAILURE);
    }
                        opts.ignore_bkps_isset = true; 

                        /* option default actions, overriden by user-defined actions below */
                            opts.ignore_bkps = !false;
                    break;
                                   case 'c':
                        optlabel = "-c/--count-only";

                            if(opts.count_only_isset) {
        error("duplicate option %s",optlabel);
        exit(EXIT_FAILURE);
    }
                        opts.count_only_isset = true; 

                        /* option default actions, overriden by user-defined actions below */
                            opts.count_only = !false;
                    break;
                                   case 'C':
                        optlabel = "-C/--color";

                            if(opts.use_color_isset) {
        error("duplicate option %s",optlabel);
        exit(EXIT_FAILURE);
    }
                        opts.use_color_isset = true; 

                        /* option default actions, overriden by user-defined actions below */
                            opts.use_color = !false;
                    break;
                                   case 'e':
                        optlabel = "-e/--exclude";

                            if(opts.filter_out_isset) {
        error("duplicate option %s",optlabel);
        exit(EXIT_FAILURE);
    }
                        opts.filter_out_isset = true; 

                        /* option default actions, overriden by user-defined actions below */
                            { free(opts . filter_out); (opts . filter_out) = NULL; }
        if(NULL != (optarg) &&       NULL == ((opts.filter_out) = strdup(optarg)))        { error0("out of memory"); exit(EXIT_FAILURE); }
                    break;
                                   case 'E':
                        optlabel = "-E/--extended-regexp";

                            if(opts.ext_regex_isset) {
        error("duplicate option %s",optlabel);
        exit(EXIT_FAILURE);
    }
                        opts.ext_regex_isset = true; 

                        /* option default actions, overriden by user-defined actions below */
                            opts.ext_regex = !false;
                    break;
                                   case 'F':
                        optlabel = "-F/--classify";

                            if(opts.classify_isset) {
        error("duplicate option %s",optlabel);
        exit(EXIT_FAILURE);
    }
                        opts.classify_isset = true; 

                        /* option default actions, overriden by user-defined actions below */
                            opts.classify = !false;
                    break;
                                   case 'i':
                        optlabel = "-i/--include";

                            if(opts.filter_in_isset) {
        error("duplicate option %s",optlabel);
        exit(EXIT_FAILURE);
    }
                        opts.filter_in_isset = true; 

                        /* option default actions, overriden by user-defined actions below */
                            { free(opts . filter_in); (opts . filter_in) = NULL; }
        if(NULL != (optarg) &&       NULL == ((opts.filter_in) = strdup(optarg)))        { error0("out of memory"); exit(EXIT_FAILURE); }
                    break;
                                   case 'I':
                        optlabel = "-I/--case-insensitive";

                            if(opts.icase_isset) {
        error("duplicate option %s",optlabel);
        exit(EXIT_FAILURE);
    }
                        opts.icase_isset = true; 

                        /* option default actions, overriden by user-defined actions below */
                            opts.icase = !false;
                    break;
                                   case 'o':
                        optlabel = "-o/--output";

                            if(opts.outfile_isset) {
        error("duplicate option %s",optlabel);
        exit(EXIT_FAILURE);
    }
                        opts.outfile_isset = true; 

                        /* option default actions, overriden by user-defined actions below */
                            { free(opts . outfile); (opts . outfile) = NULL; }
        if(NULL != (optarg) &&       NULL == ((opts.outfile) = strdup(optarg)))        { error0("out of memory"); exit(EXIT_FAILURE); }
                    break;
                                   case 'r':
                        optlabel = "-r/--regex";

                            if(opts.use_regex_isset) {
        error("duplicate option %s",optlabel);
        exit(EXIT_FAILURE);
    }
                        opts.use_regex_isset = true; 

                        /* option default actions, overriden by user-defined actions below */
                            opts.use_regex = !false;
                    break;
                                   case 'f':
                        optlabel = "-f/--only-reg";

                            if(opts.reg_only_isset) {
        error("duplicate option %s",optlabel);
        exit(EXIT_FAILURE);
    }
                        opts.reg_only_isset = true; 

                        /* option default actions, overriden by user-defined actions below */
                            opts.reg_only = !false;
                    break;
                                   case '/':
                        optlabel = "-//--only-dir";

                            if(opts.dir_only_isset) {
        error("duplicate option %s",optlabel);
        exit(EXIT_FAILURE);
    }
                        opts.dir_only_isset = true; 

                        /* option default actions, overriden by user-defined actions below */
                            opts.dir_only = !false;
                    break;
                                   case '|':
                        optlabel = "-|/--only-fifo";

                            if(opts.fifo_only_isset) {
        error("duplicate option %s",optlabel);
        exit(EXIT_FAILURE);
    }
                        opts.fifo_only_isset = true; 

                        /* option default actions, overriden by user-defined actions below */
                            opts.fifo_only = !false;
                    break;
                                   case '@':
                        optlabel = "-@/--only-lnk";

                            if(opts.lnk_only_isset) {
        error("duplicate option %s",optlabel);
        exit(EXIT_FAILURE);
    }
                        opts.lnk_only_isset = true; 

                        /* option default actions, overriden by user-defined actions below */
                            opts.lnk_only = !false;
                    break;
                                   case '=':
                        optlabel = "-=/--only-sock";

                            if(opts.sock_only_isset) {
        error("duplicate option %s",optlabel);
        exit(EXIT_FAILURE);
    }
                        opts.sock_only_isset = true; 

                        /* option default actions, overriden by user-defined actions below */
                            opts.sock_only = !false;
                    break;
                                   case '%':
                        optlabel = "-%/--only-blk";

                            if(opts.blk_only_isset) {
        error("duplicate option %s",optlabel);
        exit(EXIT_FAILURE);
    }
                        opts.blk_only_isset = true; 

                        /* option default actions, overriden by user-defined actions below */
                            opts.blk_only = !false;
                    break;
                                   case '#':
                        optlabel = "-#/--only-chr";

                            if(opts.chr_only_isset) {
        error("duplicate option %s",optlabel);
        exit(EXIT_FAILURE);
    }
                        opts.chr_only_isset = true; 

                        /* option default actions, overriden by user-defined actions below */
                            opts.chr_only = !false;
                    break;
                               }

                /* user-defined actions */
                    switch(opt) {
        case 'c':
            do_print_count = true;
            break;
            case 'f':
            type_flag |= (dt2bit[DT_REG] = (1UL << 0));
            break;
           case '/':
            type_flag |= (dt2bit[DT_DIR] = (1UL << 1));
            break;
           case '|':
            type_flag |= (dt2bit[DT_FIFO] = (1UL << 2));
            break;
           case '@':
            type_flag |= (dt2bit[DT_LNK] = (1UL << 3));
            break;
           case '=':
            type_flag |= (dt2bit[DT_SOCK] = (1UL << 4));
            break;
           case '%':
            type_flag |= (dt2bit[DT_BLK] = (1UL << 5));
            break;
           case '#':
            type_flag |= (dt2bit[DT_CHR] = (1UL << 6));
            break;
       }
            }
        }

        /* check if required options are present */
                                                                                                                                                                                                                                                                                                                                                                                 
        /* if verbose, print all option values */
        if(getopt_verbose) {
                    
    {
    verbose0("Parameter summary: --flag <bool> = value .");

    const int maxlen = 16;
    (void)maxlen;

    
        {
        char buf[1024] = "<non-representable>";
            (void)snprintf(buf,sizeof(buf),"%s",opts.print_all ? "<true>" : "<false>");
        verbose("%s-%c, --%-*s %s%s " "=" " %s%s",
                "      ",'a',maxlen,"all",
                opts.print_all_isset ? "  <set>" : "<unset>","",buf," .");
    }
       {
        char buf[1024] = "<non-representable>";
            (void)snprintf(buf,sizeof(buf),"%s",opts.almost_all ? "<true>" : "<false>");
        verbose("%s-%c, --%-*s %s%s " "=" " %s%s",
                "      ",'A',maxlen,"almost-all",
                opts.almost_all_isset ? "  <set>" : "<unset>","",buf," .");
    }
       {
        char buf[1024] = "<non-representable>";
            (void)snprintf(buf,sizeof(buf),"%s",opts.icase ? "<true>" : "<false>");
        verbose("%s-%c, --%-*s %s%s " "=" " %s%s",
                "      ",'I',maxlen,"case-insensitive",
                opts.icase_isset ? "  <set>" : "<unset>","",buf," .");
    }
       {
        char buf[1024] = "<non-representable>";
            (void)snprintf(buf,sizeof(buf),"%s",opts.classify ? "<true>" : "<false>");
        verbose("%s-%c, --%-*s %s%s " "=" " %s%s",
                "      ",'F',maxlen,"classify",
                opts.classify_isset ? "  <set>" : "<unset>","",buf," .");
    }
       {
        char buf[1024] = "<non-representable>";
            (void)snprintf(buf,sizeof(buf),"%s",opts.use_color ? "<true>" : "<false>");
        verbose("%s-%c, --%-*s %s%s " "=" " %s%s",
                "      ",'C',maxlen,"color",
                opts.use_color_isset ? "  <set>" : "<unset>","",buf," .");
    }
       {
        char buf[1024] = "<non-representable>";
            (void)snprintf(buf,sizeof(buf),"%s",opts.count_only ? "<true>" : "<false>");
        verbose("%s-%c, --%-*s %s%s " "=" " %s%s",
                "      ",'c',maxlen,"count-only",
                opts.count_only_isset ? "  <set>" : "<unset>","",buf," .");
    }
       {
        char buf[1024] = "<non-representable>";
            if(NULL == opts.filter_out) { (void)snprintf(buf,sizeof(buf),"<none>"); }
    else if(snprintf(buf,sizeof(buf),"%s",opts.filter_out) >= (int)sizeof(buf)) {
        (void)snprintf(buf + sizeof(buf) - 4,4,"...");
    }
        verbose("%s-%c, --%-*s %s%s " "=" " %s%s",
                "      ",'e',maxlen,"exclude",
                opts.filter_out_isset ? "  <set>" : "<unset>","",buf," .");
    }
       {
        char buf[1024] = "<non-representable>";
            (void)snprintf(buf,sizeof(buf),"%s",opts.ext_regex ? "<true>" : "<false>");
        verbose("%s-%c, --%-*s %s%s " "=" " %s%s",
                "      ",'E',maxlen,"extended-regexp",
                opts.ext_regex_isset ? "  <set>" : "<unset>","",buf," .");
    }
       {
        char buf[1024] = "<non-representable>";
            (void)snprintf(buf,sizeof(buf),"%s",opts.ignore_bkps ? "<true>" : "<false>");
        verbose("%s-%c, --%-*s %s%s " "=" " %s%s",
                "      ",'B',maxlen,"ignore-backups",
                opts.ignore_bkps_isset ? "  <set>" : "<unset>","",buf," .");
    }
       {
        char buf[1024] = "<non-representable>";
            if(NULL == opts.filter_in) { (void)snprintf(buf,sizeof(buf),"<none>"); }
    else if(snprintf(buf,sizeof(buf),"%s",opts.filter_in) >= (int)sizeof(buf)) {
        (void)snprintf(buf + sizeof(buf) - 4,4,"...");
    }
        verbose("%s-%c, --%-*s %s%s " "=" " %s%s",
                "      ",'i',maxlen,"include",
                opts.filter_in_isset ? "  <set>" : "<unset>","",buf," .");
    }
       {
        char buf[1024] = "<non-representable>";
            (void)snprintf(buf,sizeof(buf),"%s",opts.blk_only ? "<true>" : "<false>");
        verbose("%s-%c, --%-*s %s%s " "=" " %s%s",
                "      ",'%',maxlen,"only-blk",
                opts.blk_only_isset ? "  <set>" : "<unset>","",buf," .");
    }
       {
        char buf[1024] = "<non-representable>";
            (void)snprintf(buf,sizeof(buf),"%s",opts.chr_only ? "<true>" : "<false>");
        verbose("%s-%c, --%-*s %s%s " "=" " %s%s",
                "      ",'#',maxlen,"only-chr",
                opts.chr_only_isset ? "  <set>" : "<unset>","",buf," .");
    }
       {
        char buf[1024] = "<non-representable>";
            (void)snprintf(buf,sizeof(buf),"%s",opts.dir_only ? "<true>" : "<false>");
        verbose("%s-%c, --%-*s %s%s " "=" " %s%s",
                "      ",'/',maxlen,"only-dir",
                opts.dir_only_isset ? "  <set>" : "<unset>","",buf," .");
    }
       {
        char buf[1024] = "<non-representable>";
            (void)snprintf(buf,sizeof(buf),"%s",opts.fifo_only ? "<true>" : "<false>");
        verbose("%s-%c, --%-*s %s%s " "=" " %s%s",
                "      ",'|',maxlen,"only-fifo",
                opts.fifo_only_isset ? "  <set>" : "<unset>","",buf," .");
    }
       {
        char buf[1024] = "<non-representable>";
            (void)snprintf(buf,sizeof(buf),"%s",opts.lnk_only ? "<true>" : "<false>");
        verbose("%s-%c, --%-*s %s%s " "=" " %s%s",
                "      ",'@',maxlen,"only-lnk",
                opts.lnk_only_isset ? "  <set>" : "<unset>","",buf," .");
    }
       {
        char buf[1024] = "<non-representable>";
            (void)snprintf(buf,sizeof(buf),"%s",opts.reg_only ? "<true>" : "<false>");
        verbose("%s-%c, --%-*s %s%s " "=" " %s%s",
                "      ",'f',maxlen,"only-reg",
                opts.reg_only_isset ? "  <set>" : "<unset>","",buf," .");
    }
       {
        char buf[1024] = "<non-representable>";
            (void)snprintf(buf,sizeof(buf),"%s",opts.sock_only ? "<true>" : "<false>");
        verbose("%s-%c, --%-*s %s%s " "=" " %s%s",
                "      ",'=',maxlen,"only-sock",
                opts.sock_only_isset ? "  <set>" : "<unset>","",buf," .");
    }
       {
        char buf[1024] = "<non-representable>";
            if(NULL == opts.outfile) { (void)snprintf(buf,sizeof(buf),"<none>"); }
    else if(snprintf(buf,sizeof(buf),"%s",opts.outfile) >= (int)sizeof(buf)) {
        (void)snprintf(buf + sizeof(buf) - 4,4,"...");
    }
        verbose("%s-%c, --%-*s %s%s " "=" " %s%s",
                "      ",'o',maxlen,"output",
                opts.outfile_isset ? "  <set>" : "<unset>","",buf," .");
    }
       {
        char buf[1024] = "<non-representable>";
            (void)snprintf(buf,sizeof(buf),"%s",opts.use_regex ? "<true>" : "<false>");
        verbose("%s-%c, --%-*s %s%s " "=" " %s%s",
                "      ",'r',maxlen,"regex",
                opts.use_regex_isset ? "  <set>" : "<unset>","",buf," .");
    }
   
    
                verbose("%s    --%-*s %s%s %s %s%s","      ",
            maxlen,"color-blk",
            longopt_isset[12] ? "  <set>" : "<unset>","",
            longopt_isset[12] ? "=" : "",
            longopt_isset[12] ? longopt_arg[12] : "","");
               verbose("%s    --%-*s %s%s %s %s%s","      ",
            maxlen,"color-chr",
            longopt_isset[13] ? "  <set>" : "<unset>","",
            longopt_isset[13] ? "=" : "",
            longopt_isset[13] ? longopt_arg[13] : "","");
               verbose("%s    --%-*s %s%s %s %s%s","      ",
            maxlen,"color-dir",
            longopt_isset[8] ? "  <set>" : "<unset>","",
            longopt_isset[8] ? "=" : "",
            longopt_isset[8] ? longopt_arg[8] : "","");
               verbose("%s    --%-*s %s%s %s %s%s","      ",
            maxlen,"color-fifo",
            longopt_isset[9] ? "  <set>" : "<unset>","",
            longopt_isset[9] ? "=" : "",
            longopt_isset[9] ? longopt_arg[9] : "","");
               verbose("%s    --%-*s %s%s %s %s%s","      ",
            maxlen,"color-lnk",
            longopt_isset[10] ? "  <set>" : "<unset>","",
            longopt_isset[10] ? "=" : "",
            longopt_isset[10] ? longopt_arg[10] : "","");
               verbose("%s    --%-*s %s%s %s %s%s","      ",
            maxlen,"color-reg",
            longopt_isset[7] ? "  <set>" : "<unset>","",
            longopt_isset[7] ? "=" : "",
            longopt_isset[7] ? longopt_arg[7] : "","");
               verbose("%s    --%-*s %s%s %s %s%s","      ",
            maxlen,"color-sock",
            longopt_isset[11] ? "  <set>" : "<unset>","",
            longopt_isset[11] ? "=" : "",
            longopt_isset[11] ? longopt_arg[11] : "","");
               verbose("%s    --%-*s %s%s " "" " %s%s","      ",
            maxlen,"help",
            longopt_isset[0] ? "  <set>" : "<unset>",
            "","","");
               verbose("%s    --%-*s %s%s " "" " %s%s","      ",
            maxlen,"no-classify",
            longopt_isset[3] ? "  <set>" : "<unset>",
            "","","");
               verbose("%s    --%-*s %s%s " "" " %s%s","      ",
            maxlen,"no-color",
            longopt_isset[4] ? "  <set>" : "<unset>",
            "","","");
               verbose("%s    --%-*s %s%s " "" " %s%s","      ",
            maxlen,"no-fallback",
            longopt_isset[5] ? "  <set>" : "<unset>",
            "","","");
               verbose("%s    --%-*s %s%s " "" " %s%s","      ",
            maxlen,"print-count",
            longopt_isset[6] ? "  <set>" : "<unset>",
            "","","");
               verbose("%s    --%-*s %s%s " "" " %s%s","      ",
            maxlen,"usage",
            longopt_isset[1] ? "  <set>" : "<unset>",
            "","","");
               verbose("%s    --%-*s %s%s " "" " %s%s","      ",
            maxlen,"verbose",
            longopt_isset[2] ? "  <set>" : "<unset>",
            "","","");
   }
        }

        const size_t len_optab_long = 14;
        {
const size_t _len = len_optab_long;
for(size_t _idx = 0; _idx < _len; ++_idx) {
const size_t _0 = _idx;
const size_t _idx = 0; (void)_idx;
{ free(( ( ( longopt_arg ) ) [ _0 ] )); (( ( ( longopt_arg ) ) [ _0 ] )) = NULL; }}
}
    }

    char* (*strstr_f)(const char *, const char *) = opts.icase
                                                  ? strcasestr : strstr;

        regex_t reg_in;
       regex_t reg_out;
       if(opts.use_regex) {
                if(opts.filter_in_isset) {
            int flags = REG_NOSUB
                | (opts.ext_regex ? REG_EXTENDED : 0)
                | (opts.icase ? REG_ICASE : 0);
            int ret = regcomp(&reg_in, opts.filter_in,flags);
            if(ret) {
                size_t bufsiz = regerror(ret,&reg_in,NULL,0);
                char buf[bufsiz]; regerror(ret,&reg_in,buf,bufsiz);
                error("Regex compilation (" "include" ") failed with error: %s",buf);
                exit(EXIT_FAILURE);
            }
        }
               if(opts.filter_out_isset) {
            int flags = REG_NOSUB
                | (opts.ext_regex ? REG_EXTENDED : 0)
                | (opts.icase ? REG_ICASE : 0);
            int ret = regcomp(&reg_out, opts.filter_out,flags);
            if(ret) {
                size_t bufsiz = regerror(ret,&reg_out,NULL,0);
                char buf[bufsiz]; regerror(ret,&reg_out,buf,bufsiz);
                error("Regex compilation (" "exclude" ") failed with error: %s",buf);
                exit(EXIT_FAILURE);
            }
        }
           }

    errno = 0;
    FILE *fpout = opts.outfile_isset ? fopen(opts.outfile,"w") : stdout;
    if(fpout == NULL) {
        error("Failed opening file \"%s\" for write: %s (%d)",
                opts.outfile ? opts.outfile : "<null>",
                strerror(errno),errno);
        if(fp_fallback) {
            warn0("Using standard output as fallback");
            fpout = stdout;
        } else exit(EXIT_FAILURE);
    }

    size_t count = 0;

    // Directory to open, first argument or default to "."
    const char *lfdir = optind < argc ? argv[optind] : ".";

    errno = 0;
    DIR *handle = opendir(lfdir);
    if(NULL == handle) {
    error("%s: %s (%d)","opendir",strerror(errno),errno);
    exit(EXIT_FAILURE);
}


    errno = 0;
    struct dirent *dent = NULL;

    while(NULL != (dent = readdir(handle))) {
        int d_type = dent->d_type;
        const char *d_name = dent->d_name;

                                        /* the snippets above are used here */
        if((type_flag && !(type_flag & dt2bit[d_type])) || (            '.' == d_name[0] && (
                    (!opts.print_all && !opts.almost_all)
                        || (opts.almost_all && ('\0' == d_name[1] || ('.' == d_name[1] && '\0' == d_name[2]))))
       ) || (opts.ignore_bkps && '~' == d_name[strlen(d_name) - 1]))
            continue;

                if(opts.filter_in_isset &&
                ((opts.use_regex && regexec(&reg_in,d_name,0,NULL,0))
                 || (!opts.use_regex && !strstr_f(d_name,opts.filter_in))))
            continue;
               if(opts.filter_out_isset &&
                ((opts.use_regex && !regexec(&reg_out,d_name,0,NULL,0))
                 || (!opts.use_regex && strstr_f(d_name,opts.filter_out))))
            continue;
       
        count++; // we got here, so count the file

        if(!opts.count_only)
            fprint_pretty(fpout,d_name,d_type,opts.classify,opts.use_color,custom_colors);

        errno = 0;
    }
    if(errno != 0) {
    error("%s: %s (%d)","readdir",strerror(errno),errno);
    exit(EXIT_FAILURE);
}


    if(do_print_count) printf("%zu\n",count);

    /* cleanup */
    closedir(handle);
    if(fpout != stdout) fclose(fpout);
    if(opts.filter_in_isset) regfree(&reg_in);
    if(opts.filter_out_isset) regfree(&reg_out);
                                                                           { free(opts . filter_out); (opts . filter_out) = NULL; }
                                     { free(opts . filter_in); (opts . filter_in) = NULL; }
                          { free(opts . outfile); (opts . outfile) = NULL; }
                                                                                           }
