# listdir, a.k.a `lf`

+ List tens of thousands of files fast!
+ Count number of files, no more `ls | wc -l`
+ Filter files by type and name (string or regex)
+ Works on GNU/Linux, macOS and BSD (untested)
+ Particularly useful in HPC environments (GPFS, etc.)
+ Use as input to GNU parallel for distributed processing
